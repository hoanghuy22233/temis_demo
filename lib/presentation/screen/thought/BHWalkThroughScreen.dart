import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:ybc/app/constants/barrel_constants.dart';
import 'package:ybc/utils/more/BHColors.dart';
import 'package:ybc/utils/more/BHConstants.dart';

class BHWalkThroughScreen extends StatefulWidget {
  static String tag = '/WalkThroughScreen';

  @override
  BHWalkThroughScreenState createState() => BHWalkThroughScreenState();
}

class BHWalkThroughScreenState extends State<BHWalkThroughScreen> {
  PageController _pageController = PageController();
  int currentPage = 0;
  static const _kDuration = const Duration(seconds: 1);
  static const _kCurve = Curves.ease;
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Scaffold(
          body: SafeArea(
            top: true,
            child: _buildContent(),
          )
        )
    );
  }

  _saveCountry() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(AppPreferences.LOGIN, "Login");
  }

  Widget _buildContent() {
    return Stack(
      children: [
        PageView(
          controller: _pageController,
          onPageChanged: (i) {
            currentPage = i;
            setState(() {});
          },
          children: [
            Image.asset('assets/images/banner1.png',
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.fill),
            Image.asset('assets/images/banner2.png',
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.fill),
            Image.asset('assets/images/banner3.png',
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.fill),
          ],
        ),
        Positioned(
          bottom: 90,
          child: Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width,
            child: DotsIndicator(
              dotsCount: 3,
              position: currentPage,
              decorator: DotsDecorator(
                color: BHGreyColor.withOpacity(0.5),
                activeColor: BHColorPrimary,
                size: Size.square(9.0),
                activeSize: Size(18.0, 9.0),
                activeShape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0)),
              ),
            ),
          ),
        ),
        Positioned(
          bottom: 16,
          left: 0,
          right: 0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              FlatButton(
                child: Text('Bỏ qua',
                    style: TextStyle(color: Colors.white, fontSize: 16)),
                onPressed: () {
                  // finish(context);
                  // BHLoginScreen().launch(context);
                  AppNavigator.navigateLogin();
                  _saveCountry();
                },
              ),
              FlatButton(
                child: Text(BHBtnNext,
                    style: TextStyle(color: Colors.white,  fontSize: 16)),
                onPressed: () {
                  _pageController.nextPage(
                      duration: _kDuration, curve: _kCurve);
                },
              )
            ],
          ).visible(
            currentPage != 2,
            defaultWidget: Container(
              margin: EdgeInsets.only(),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: 270,
                  height: 40,
                  child: RaisedButton(
                    onPressed: () {
                      AppNavigator.navigateLogin();
                      _saveCountry();
                    },
                    color: BHColorPrimary,
                    child: Text(BHBtnGetStarted,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: whiteColor)),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
