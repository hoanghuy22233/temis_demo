import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:ybc/app/constants/style/style.dart';

class TemplatePlanBusinessCreatedScreen extends StatefulWidget {
  @override
  _TemplatePlanBusinessCreatedScreenState createState() => _TemplatePlanBusinessCreatedScreenState();
}

class _TemplatePlanBusinessCreatedScreenState extends State<TemplatePlanBusinessCreatedScreen> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: RefreshIndicator(
            onRefresh: () async {
              await Future.delayed(Duration(seconds: 3));
              return true;
            },
            color: Colors.blue,
            backgroundColor: Colors.white,
            child: Container(
                color: Colors.grey[100],
                child: _buildContent()
            )
        )
    );
  }

  _buildContent(){
    return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    height: 120,
                    width: 120,
                    child: Image.asset('assets/images/box.png')
                ),
                SizedBox(height: 20,),
                Text('Bạn chưa tạo mẫu kế hoạch kinh doanh!'),
              ],
            ),
    );
  }
}
