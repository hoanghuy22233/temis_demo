import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:ybc/app/constants/color/color.dart';
import 'package:ybc/model/entity/category_menu.dart';
import 'package:ybc/model/entity/home_data.dart';
import 'package:ybc/model/repo/home_repository.dart';
import 'package:ybc/presentation/common_widgets/widget_appbar_with_search.dart';
import 'package:ybc/presentation/common_widgets/widget_screen_error.dart';
import 'package:ybc/presentation/screen/work_home/menu/profile/widget_cart_appbar_menu.dart';

class WorkCourseScreen extends StatefulWidget {
  MenuCategories category;
  int type;

  WorkCourseScreen({this.category, this.type});
  @override
  _WorkCourseScreenState createState() => _WorkCourseScreenState();
}

class _WorkCourseScreenState extends State<WorkCourseScreen>
    with AutomaticKeepAliveClientMixin<WorkCourseScreen> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  String dropdownValue;
  List<Item> _item = Item.getItem();
  List<DropdownMenuItem<Item>> _dropdownMenuItems;
  Item _selected;
  HomeData dropdownValues;
  List<HomeData> homeDatas;
  int type;

  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    openLoading();
    var homeRepository = RepositoryProvider.of<HomeRepository>(context);
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: SafeArea(
          top: true,
          child: Column(
            children: [
              _buildAppbar("Khóa học", "Tìm kiếm khóa học"),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                height: 35,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 10,
                    ),
                    Image.asset(
                      'assets/images/ic_grid.png',
                      height: 15,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    DropdownButton<Item>(
                      hint: Text("Sắp xếp theo"),
                      value: _selected,
                      icon: Icon(Icons.arrow_drop_down),
                      iconSize: 24,
                      elevation: 16,
                      underline: SizedBox(),
                      style: TextStyle(color: Colors.black),
                      onChanged: (Item value) {
                        setState(() {
                          _selected = value;

                          print("____________");
                          print(_selected.key);
                        });
                      },
                      items: _item.map((Item user) {
                        return DropdownMenuItem<Item>(
                          value: user,
                          child: Text(
                            user.name,
                            style: TextStyle(color: Colors.black),
                          ),
                        );
                      }).toList(),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Expanded(
                child: Center(
                  child: Container(
                    child: Text('Khóa học'),
                  ),
                ),
              )
            ],
          )),
    );
      // MultiBlocProvider(
      //   providers: [
      //     BlocProvider(
      //       create: (context) =>
      //           CategoryCourseBloc(homeRepository: homeRepository),
      //     ),
      //   ],
      //   child: BlocListener<CoursesBloc, CoursesState>(
      //     listener: (context, state) async {
      //       if (state.isLoading) {
      //         await HttpHandler.resolve(status: state.status);
      //       }
      //
      //       if (state.isSuccess) {
      //         await HttpHandler.resolve(status: state.status);
      //       }
      //
      //       if (state.isFailure) {
      //         await HttpHandler.resolve(status: state.status);
      //       }
      //     },
      //     child: BlocBuilder<CoursesBloc, CoursesState>(
      //       builder: (context, state) {
      //         return Scaffold(
      //           backgroundColor: Colors.grey[300],
      //           body: SafeArea(
      //               top: true,
      //               child: Column(
      //                 children: [
      //                   _buildAppbar("Khóa học", "Tìm kiếm khóa học"),
      //                   Container(
      //                     margin: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
      //                     height: 35,
      //                     child: Row(
      //                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //                       children: [
      //                         Expanded(
      //                             flex: 6,
      //                             child: Container(
      //                               padding:
      //                                   EdgeInsets.symmetric(horizontal: 15),
      //                               decoration: BoxDecoration(
      //                                 borderRadius: BorderRadius.circular(25),
      //                                 color: Colors.white,
      //                               ),
      //                               child: WidgetCourserCategories(
      //                                 type: type,
      //                               ),
      //                             )),
      //                         SizedBox(
      //                           width: 10,
      //                         ),
      //                         Image.asset(
      //                           'assets/images/ic_grid.png',
      //                           height: 15,
      //                         ),
      //                         SizedBox(
      //                           width: 10,
      //                         ),
      //                         DropdownButton<Item>(
      //                           hint: Text("Sắp xếp theo"),
      //                           value: _selected,
      //                           icon: Icon(Icons.arrow_drop_down),
      //                           iconSize: 24,
      //                           elevation: 16,
      //                           underline: SizedBox(),
      //                           style: TextStyle(color: Colors.black),
      //                           onChanged: (Item value) {
      //                             setState(() {
      //                               _selected = value;
      //                               BlocProvider.of<CoursesBloc>(context).add(
      //                                   LoadCourses(
      //                                       type ?? null,
      //                                       _selected?.key ?? '',
      //                                       _selected?.key ?? ''));
      //                               print("____________");
      //                               print(_selected.key);
      //                             });
      //                           },
      //                           items: _item.map((Item user) {
      //                             return DropdownMenuItem<Item>(
      //                               value: user,
      //                               child: Text(
      //                                 user.name,
      //                                 style: TextStyle(color: Colors.black),
      //                               ),
      //                             );
      //                           }).toList(),
      //                         )
      //                       ],
      //                     ),
      //                   ),
      //                   SizedBox(
      //                     height: 10,
      //                   ),
      //                   Expanded(
      //                     child: _buildContent(state),
      //                   )
      //                 ],
      //               )),
      //         );
      //       },
      //     ),
      //   ));
  }


  @override
  bool get wantKeepAlive => true;

  Widget _buildAppbar(String title, String titleSearch) =>
      WidgetAppbarWithSearch(
        backgroundColor: AppColor.PRIMARY_COLOR,
        textColor: Colors.white,
        title: title,
        titleSearch: titleSearch,
        right: [
          WidgetCartAppBarMenu()
        ],
      );



  void openLoading() async {
    Future.delayed(Duration(seconds: 2), () {

    });
  }
}

class Item {
  const Item(this.id, this.name, this.key);
  final int id;
  final String name;
  final String key;

  static List<Item> getItem() {
    return <Item>[
      Item(0, 'Lọc theo giá', 'final_price|asc'),
      Item(1, 'Lọc theo ngày', "created_at|desc"),
    ];
  }
}
